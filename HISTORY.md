# Release history

## 0.6.0 (2021-02-02)

* Bumped to kadi-apy 0.12.0.

## 0.5.0 (2021-05-10)

* Add matlab wrapper node.
* Add wrapper node for xmllint.
* Add handling of exit code to RunScript node.

## 0.4.1 (2021-04-26)

* Add pypi deploy runner.

## 0.4.0 (2021-04-22)

* Add autocompletion.
* Add xml_to_kadi converter.

## 0.3.0 (2021-02-26)

* Add a node for histograms.
* Add simple smtp node.
* Bumped the `kadi-apy` dependency to the correct version.
* Adjust record visualization tools.

## 0.2.1 (2021-02-03)

* Bumped the `kadi-apy` dependency to the correct version.

## 0.2.0 (2021-02-02)

* Added a tool to run a shell script using CMD or Powershell via the WSL.
* Added a tool to merge tables.
* Added a general file converter tool.
* Added tools to visualize records in a Kadi4Mat instance.
* Added a tool for combining files.
* Added a tool to create QR codes.
* Removed the basic Kadi4Mat integration tools, which are replaced by the CLI
  provided by the `kadi-apy` itself.
* Refactored all existing tools to use a single parent command
  `workflow-nodes`.
* Various other fixes and refactorings.

## 0.1.0 (2020-10-16)

* The first few tools for various common use-cases are provided, including
  tools for converting, plotting and analysing data, for generating reports and
  for connecting to a Kadi4Mat instance.
