# Copyright 2021 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import subprocess
import sys

from xmlhelpy import argument
from xmlhelpy import Choice
from xmlhelpy import option

from .main import system


@system.command(
    version="0.1.0",
    description="Octave node for using octave scripts",
)
@argument("file", required=True)
@option(
    "exec-path",
    var_name="path",
    char="p",
    description="Set the execution path in which to look for the octave script",
)
@option(
    "execution-mode",
    char="m",
    var_name="mode",
    description="Choose the desired execution mode:"
    " single=single execution of script in bash"
    " and Matlab closes automatically after runthrough;"
    " bash=bash execution of script and matlab stays open after runthrough;"
    " desktop=execution of script in Matlab desktop"
    " and matlab stays open after runthrough",
    default="single",
    param_type=Choice(["single", "bash", "desktop"]),
)
def octave(file, path, mode):
    """Wrapper node for Octave."""

    cmd = ["octave"]
    if path:
        cmd += [path, file]
    if mode == "single":
        cmd += ["--no-gui"]
    elif mode == "bash":
        cmd += ["--no-gui --persist"]
    elif mode == "desktop":
        cmd += ["--persist"]

    print(" ".join(cmd), file=sys.stderr)
    sys.exit(subprocess.call(cmd))
