# workflow-wodes

**workflow-nodes** is a collection of nodes usable inside a *workflow* written
in Python 3. Each node is an executable command line tool providing the
`--xmlhelp` interface, which can be used to obtain a machine readable
representation of any command line tool and its parameters (see also
[xmlhelpy](https://gitlab.com/iam-cms/workflows/xmlhelpy).

There are nodes to generate reports, to wrap system commands like `awk` and
some nodes for special use cases like integrating ImageJ macros. Additionally,
some nodes can interact with a [Kadi4Mat](https://kadi.iam-cms.kit.edu/)
instance as well.

## Installation

The library can be installed using `pip3` (or `pip`, if there is no dedicated
version of `pip` for Python 3 installed), which generally comes bundled with
Python installations. Python version >= 3.6 is required.

`pip3 install workflow-nodes`

When installing the library from source for development instead, it is
recommended to install the library in editable mode, which simply creates a
link to the sources so all changes are reflected in the installed package
immediately. The command will also install some additional development
dependencies as defined in `setup.py`.

`pip3 install -e .[dev]`

Running the installations inside a virtual environment is recommended, see
[Virtualenv](https://virtualenv.pypa.io/en/latest/ "Virtualenv") for more
information.

After installing, make sure the `workflow-nodes` command is runnable from
anywhere. Depending on the type of installation and if a virtual environment is
used or not, the `PATH` system variable may need to be adapted accordingly to
include the path the executable resides in. For example, the following command
can be used on Linux, assuming the executable resides in `~/.local/bin`:

`export PATH=${HOME}/.local/bin:${PATH}`

This line can also be added to `.bashrc` so it will be executed each time a new
terminal is opened:

`echo 'export PATH=${HOME}/.local/bin:${PATH}' >> ${HOME}/.bashrc`

Autocomplete is available for fish, zsh, bash and PowerShell and can be
activated via:

`workflow-nodes config activate-autocompletion`

## Usage

All commands concerning different resources are available as various
subcommands. For more information or to get a list of subcommands, the
following commands can be used:

`workflow-nodes --help`

`workflow-nodes --commands`

To use nodes with the local workflow editor, they have to be added to the file
`$HOME/.pacestudio/tools.txt`. Afterwards you can insert them using the "Add
Tool" context menu option in the editor.

## Contributing

* First, create a fork of this project and clone it. The main repository can be
  added as an additional remote to the cloned project, often called `upstream`.
  Also checkout the `development` branch, which is currently used as the main
  development branch.
* Install the project in development mode according to the instructions above
  and setup the [pre-commit](https://pre-commit.com/) hooks by running
  `pre-commit install`.
* Create a Python file for the node in the desired package, e.g. in
  `workflow_nodes/report/` and implement the functionality of the tool. You can
  use other existing tools as a reference. Afterwards, add the implemented
  function to a fitting subcommand group, e.g. to
  `workflow_nodes/report/main.py`.
* Before creating a merge request on the main repository, make sure the GitLab
  CI runs through successfully in your fork.
